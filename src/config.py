#!/builds/orchestrator/bin/python

"""
This script is run first by `gitlab-runner` and prints a JSON
configuration, with a `job_env` dictionary which specifies
variables that will be able in the environment of the
following scripts `prepare.py`, `run.py` and `cleanup.py`.

This script handles two tasks:

(1) This script parses the value of the environment variable
   `CI_JOB_IMAGE` (that is to say, the value if the job `image:` key)
   into a `base.Image` value, so that the parsing is only done
   once. This value is then serialized as a JSON string into the
   environment variable `image` (this value will be then deserialized,
   that is to say parsed, again, but at least the logic to compute it
   is only executed once, and we hope that the JSON parser is more
   efficient than our Python parser!).

(2) We use this script to overcome a limitation of GitLab Custom
   executors, which have no access to the actual tags of the running
   job, and they have no access to the `entrypoint` that can be
   specified for the `image`.  In this script, we try to recompute the
   configuration of the job by reparsing the file .gitlab-ci.yml, that
   we download from GitLab, using the job token. We reimplement a part
   of the GitLab logic for `include`, `extends` and `variables`.

   See: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27301

The task (1) relies on task (2) because the image can be deduced from
tags.
"""

# Standard library
import json
import logging
import os
import re
import typing
import urllib.parse

# External dependencies
import pyparsing
import requests
import yaml

# Project module
import base

SPEC_TAGS = {
    "small": base.Spec(cpu=1, ram=2, disk=16),
    "medium": base.Spec(cpu=2, ram=4, disk=16),
    "large": base.Spec(cpu=4, ram=8, disk=16),
}

OS_TAGS = {
    "linux": "community:ci.inria.fr/ubuntu-23.04/amd64",
    "macos": "featured:CI-MacOSX-Catalina-10.15.4-xcode",
    "windows": "community:windows-10-wsl",
}


def main(logger: logging.LoggerAdapter):
    job_env = {}
    tags, entrypoint = recompute_tags_and_entrypoint(logger)
    if entrypoint is not None:
        job_env["entrypoint"] = json.dumps(entrypoint)
    image = compute_image(logger, tags, os.environ.get("CUSTOM_ENV_CI_JOB_IMAGE"))
    job_env["image"] = image.model_dump_json()
    response = {"job_env": job_env}
    print(json.dumps(response))


def recompute_tags_and_entrypoint(
    logger: logging.LoggerAdapter,
) -> tuple[set[str], typing.Optional[list[str]]]:
    context = {
        key.removeprefix("CUSTOM_ENV_"): value
        for key, value in os.environ.items()
        if key.startswith("CUSTOM_ENV_")
    }
    spec = recompute_spec(logger, context)
    ci_job_name = os.environ["CUSTOM_ENV_CI_JOB_NAME"]
    spec_context = get_variables(spec, context)
    job_spec = spec.get(ci_job_name)
    if job_spec is None:
        job_spec = {}
    else:
        extend_job_spec(logger, job_spec, spec)
    variables = get_variables(job_spec, spec_context)
    tags = job_spec.get("tags")
    tags = expand_variables(logger, tags, variables)
    image = job_spec.get("image")
    if image is None:
        image = spec.get("image")
    if type(image) is dict:
        entrypoint = image.get("entrypoint")
        if entrypoint is not None:
            assert isinstance(entrypoint, list) and all(
                isinstance(item, str) for item in entrypoint
            )
    else:
        entrypoint = None
    entrypoint = expand_variables(logger, entrypoint, variables)
    return set(tags), entrypoint


def recompute_spec(logger: logging.LoggerAdapter, context):
    ci_repository_url = os.environ["CUSTOM_ENV_CI_REPOSITORY_URL"]
    ci_commit_sha = os.environ["CUSTOM_ENV_CI_COMMIT_SHA"]
    ci_config_path = os.environ["CUSTOM_ENV_CI_CONFIG_PATH"]
    repository = ci_repository_url.removesuffix(".git")
    url = get_gitlab_file_url(repository, ci_commit_sha, ci_config_path)
    return load_spec(logger, url, repository, ci_commit_sha, context, False)


def get_gitlab_file_url(repository, ref, path):
    return f"{repository}/-/raw/{ref}/{path}"


def load_spec(logger: logging.LoggerAdapter, url, repository, sha, context, merging):
    logger.info(f"Fetching pipeline specification: {url}")
    response = requests.get(url)
    response.raise_for_status()
    spec = yaml.safe_load(response.text)
    variables = get_variables(spec, context, merging=merging)
    include_specs(logger, spec, repository, sha, variables)
    return spec


def update_undefined(target, source):
    for key, value in source.items():
        if key not in target:
            target[key] = value
        elif type(value) is dict:
            target_value = target[key]
            if type(target_value) is dict:
                update_undefined(target_value, value)


def get_variables(spec, context, merging=False):
    new_variables = spec.get("variables", {})
    if merging:
        new_variables.update(context)
    else:
        update_undefined(new_variables, context)
    return new_variables


# The support for variables has been removed from `extend_job_spec` since
# GitLab does not support variables in `extends`.
# https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html
def extend_job_spec(logger: logging.LoggerAdapter, job_spec, spec):
    """Expands all the `extends` recursively in the specification of a job.

    - `job_spec` is the dictionary specifying the job we are interested in,
       (this dictionary possibly contains an `extends` key, which will be
       expanded);
    - `spec` is a dictionary where the specification of the jobs referred
       in `extends` can be found; in practice, it is the contents of
       `.gitlab-ci.yml` itself, where `includes` have been solved with
       `recompute_spec`).

    >>> logger = logging.LoggerAdapter(logging.getLogger())
    >>> spec = {".large": {"tags": ["large"]}, ".small": {"tags": ["small"]}}
    >>> job = {"extends": ".large"}
    >>> extend_job_spec(logger, job, spec)
    >>> job
    {'extends': '.large', 'tags': ['large']}
    >>> job = {"extends": [".large", ".small"]}
    >>> extend_job_spec(logger, job, spec)
    >>> job
    {'extends': ['.large', '.small'], 'tags': ['small']}
    >>> spec = {".large": {"tags": ["large"]}, ".large2": {"extends": ".large"}}
    >>> job = {"extends": ".large2"}
    >>> extend_job_spec(logger, job, spec)
    >>> job
    {'extends': '.large2', 'tags': ['large']}
    """
    extends = job_spec.get("extends", [])
    if type(extends) is not list:
        extends = [extends]
    # The list of extends is iterated in reverse order.
    # This should fix https://gitlab.inria.fr/inria-ci/custom-runner/-/issues/10
    # where tags were defined in two hidden jobs:
    #
    #   .ci-template-flambda:
    #      tags: [inria-ci/custom-runner] # computed via other extends...
    #   .large-tags:
    #      tags: [large]
    #   library:ci-fiat_crypto:
    #     extends:
    #       - .ci-template-flambda
    #       - .large-tags
    #
    # The expected tag list is `[large]`, meaning the second item in
    # `extends`, `.large-tags`, should take precedence over the first
    # one, `.ci-template-flambda`. However, we extend a specification
    # by using `update_undefined`, which only takes fields that are
    # not already specified (because if a field is directly defined in
    # the job, here `library:ci-fiat_crypto`, it overrides the fields
    # defined in extended jobs). Therefore, values set first override
    # subsequent ones. To ensure the last extended job overrides the
    # first, we must apply the last extended job first.
    for name in reversed(extends):
        other_spec = spec.get(name)
        if other_spec is None:
            logger.warning(f"Ignored extend: {name}")
            continue
        extend_job_spec(logger, other_spec, spec)
        update_undefined(job_spec, other_spec)


def include_specs(
    logger: logging.LoggerAdapter, spec, repository: str, sha: str, context
) -> None:
    include = spec.get("include", [])
    if type(include) is not list:
        include = [include]
    for item in include:
        urls = get_include_urls(logger, item, repository, sha, context)
        if urls is None:
            logger.warning(f"Ignored include: {item}")
            continue
        for url in urls:
            other_spec = load_spec(logger, url, repository, sha, context, True)
            update_undefined(spec, other_spec)


def get_include_urls(
    logger: logging.LoggerAdapter, item, repository: str, sha: str, context
) -> typing.Optional[list[str]]:
    """
    Compute URLs for include items.

    >>> logger = logging.LoggerAdapter(logging.getLogger())
    >>> get_include_urls(logger, {'template':  'Terraform/Base.gitlab-ci.yml'}, "https://gitlab.inria.fr/a/b", "main", {})
    ['https://gitlab.com/gitlab-org/gitlab/-/raw/master/lib/gitlab/ci/templates/Terraform/Base.gitlab-ci.yml']
    >>> get_include_urls(logger, {'local':  '/sub.yml'}, "https://gitlab.inria.fr/a/b", "main", {})
    ['https://gitlab.inria.fr/a/b/-/raw/main/sub.yml']
    >>> get_include_urls(logger, '/sub.yml', "https://gitlab.inria.fr/a/b", "main", {})
    ['https://gitlab.inria.fr/a/b/-/raw/main/sub.yml']
    >>> get_include_urls(logger, {'project':  'group/project', 'ref': 'branch', 'file': '/sub.yml'}, "https://gitlab.inria.fr/a/b", "main", {})
    ['https://gitlab.inria.fr/group/project/-/raw/branch/sub.yml']
    >>> get_include_urls(logger, {'project':  'group/project', 'file': ['/a.yml', '/b.yml']}, "https://gitlab.inria.fr/a/b", "main", {})
    ['https://gitlab.inria.fr/group/project/-/raw/main/a.yml', 'https://gitlab.inria.fr/group/project/-/raw/main/b.yml']
    >>> get_include_urls(logger, {'remote':  'https://gitlab.com/a/b/sub.yml'}, "https://gitlab.inria.fr/a/b", "main", {})
    ['https://gitlab.com/a/b/sub.yml']
    >>> get_include_urls(logger, 'https://gitlab.com/a/b/sub.yml', "https://gitlab.inria.fr/a/b", "main", {})
    ['https://gitlab.com/a/b/sub.yml']
    >>> get_include_urls(logger, 'https://test.evil/a/b/sub.yml', "https://gitlab.inria.fr/a/b", "main", {})
    """
    if type(item) is dict:
        if rules := item.get("rules"):
            if not check_rules(logger, rules, item, context):
                return []
        if template := item.get("template"):
            template = expand_variables(logger, template, context)
            return [
                "https://gitlab.com/gitlab-org/gitlab/-/raw/master/lib/gitlab/ci/templates/"
                + template
            ]
        elif local := item.get("local"):
            local = expand_variables(logger, local, context)
            if local.startswith("/"):
                return [get_gitlab_file_url(repository, sha, local.removeprefix("/"))]
        elif remote := item.get("remote"):
            remote = expand_variables(logger, remote, context)
            if is_url_allowed(remote):
                return [remote]
        elif project := item.get("project"):
            project = expand_variables(logger, project, context)
            parsed_repository_url = urllib.parse.urlparse(repository)
            projects_prefix = (
                f"{parsed_repository_url.scheme}://{parsed_repository_url.netloc}"
            )
            project_prefix = f"{projects_prefix}/{project}"
            urls = []
            ref = item.get("ref", "main")
            files = item.get("file", [])
            if type(files) is not list:
                files = [files]
            for f in files:
                if f.startswith("/"):
                    urls.append(
                        get_gitlab_file_url(project_prefix, ref, f.removeprefix("/"))
                    )
                else:
                    logger.warning(f"Ignored include: {f}")
            return urls
    elif type(item) is str:
        item = expand_variables(logger, item, context)
        if item.startswith("/"):
            return [get_gitlab_file_url(repository, sha, item.removeprefix("/"))]
        elif is_url_allowed(item):
            return [item]
    return None


class String(typing.NamedTuple):
    value: str


class Variable(typing.NamedTuple):
    name: str


class Regex(typing.NamedTuple):
    value: re.Pattern


VARIABLE_REGEX = re.compile("[$]((?P<name>[A-Za-z0-9_]+)|{(?P<curlyname>[^}]+)})")

REGEX_REGEX = re.compile("/(?P<regex>[^/]*)/")

CONDITIONAL_OPERAND = (
    pyparsing.QuotedString(quoteChar='"', unquoteResults=True).set_parse_action(
        lambda s, l, t: String(t[0])
    )
    | pyparsing.Regex(VARIABLE_REGEX).set_parse_action(
        lambda s, l, t: Variable(t["name"] or t["curlyname"])
    )
    | pyparsing.Regex(REGEX_REGEX).set_parse_action(
        lambda s, l, t: Regex(re.compile(t["regex"]))
    )
)

CONDITIONAL = pyparsing.infixNotation(
    CONDITIONAL_OPERAND,
    [
        (pyparsing.oneOf("== != =~ !~"), 2, pyparsing.opAssoc.LEFT),
        ("&&", 2, pyparsing.opAssoc.LEFT),
        ("||", 2, pyparsing.opAssoc.LEFT),
    ],
)


def check_rules(logger: logging.LoggerAdapter, rules, item, context):
    for rule in rules:
        if condition_str := rule.get("if"):
            condition = CONDITIONAL.parseString(condition_str)
            result = evaluate_condition(logger, condition, context)
        elif files := rule.get("exists"):
            logger.warning(
                "include:rules:exists are currently ignored in custom runners."
            )
            result = True
        else:
            result = True
        if result:
            if rule.get("when") == "never":
                return False
            else:
                return True


def evaluate_condition(logger: logging.LoggerAdapter, condition, context):
    r"""
    Evaluate rule condition.

    >>> logger = logging.LoggerAdapter(logging.getLogger())
    >>> evaluate_condition(logger, CONDITIONAL.parseString("\"a\" == \"a\""), {})
    True
    >>> evaluate_condition(logger, CONDITIONAL.parseString("\"a\" == \"a\" && \"b\" == \"b\""), {})
    True
    >>> evaluate_condition(logger, CONDITIONAL.parseString("\"a\" == \"b\""), {})
    False
    >>> evaluate_condition(logger, CONDITIONAL.parseString("\"a\" != \"a\""), {})
    False
    >>> evaluate_condition(logger, CONDITIONAL.parseString("\"a\" != \"b\""), {})
    True
    >>> evaluate_condition(logger, CONDITIONAL.parseString("\"a\" != $a"), {"a": "a"})
    False
    >>> evaluate_condition(logger, CONDITIONAL.parseString("\"a\" == $a"), {"a": "a"})
    True
    >>> evaluate_condition(logger, CONDITIONAL.parseString("\"a\" =~ /a/"), {})
    True
    >>> evaluate_condition(logger, CONDITIONAL.parseString("\"a\" =~ $a"), {"a": "/a/"})
    True
    """
    match condition:
        case String(value=value):
            return value
        case Variable(name=name):
            return lookup_variable(logger, name, context)
        case Regex(value=value):
            return value
        case [lhs, operator, rhs]:
            # See https://github.com/python/mypy/issues/14485
            # False unreachable warning on match statement
            lhs = evaluate_condition(logger, lhs, context)  # type: ignore[unreachable]
            rhs = evaluate_condition(logger, rhs, context)
            match operator:
                case "==":
                    return lhs == rhs
                case "!=":
                    return lhs != rhs
                case "=~":
                    # If lhs is an undefined variable, lhs will be None
                    # and match(lhs) will raise a type error.
                    # See https://gitlab.inria.fr/coq/coq/-/jobs/3479427
                    if not isinstance(lhs, str):
                        return False
                    return ensure_regexp(logger, rhs).match(lhs) is not None
                case "!~":
                    if not isinstance(lhs, str):
                        return False
                    return ensure_regexp(logger, rhs).match(lhs) is None
                case "&&":
                    return lhs and rhs
                case "||":
                    return lhs or rhs
        case [e]:
            return evaluate_condition(logger, e, context)  # type: ignore[unreachable]
    logger.warning(f"Cannot evaluate: {condition}")
    return False


def ensure_regexp(
    logger: logging.LoggerAdapter, regexp: re.Pattern | str
) -> re.Pattern:
    if isinstance(regexp, re.Pattern):
        return regexp
    m = REGEX_REGEX.match(regexp)
    if m is None:
        logger.warning(f"Not a regular expression: {regexp}")
        return re.compile(regexp)
    return re.compile(m.group("regex"))


def is_url_allowed(url):
    return any(
        url.startswith(allowed)
        for allowed in ["https://gitlab.inria.fr/", "https://gitlab.com/"]
    )


T = typing.TypeVar("T")


def expand_variables(logger: logging.LoggerAdapter, v: T, context) -> T:
    """
    Expand variables in a value.

    >>> logger = logging.LoggerAdapter(logging.getLogger())
    >>> expand_variables(logger, "$x", {"x": "value"})
    'value'
    >>> expand_variables(logger, "a $x b $y c", {"x": "X", "y": "Y"})
    'a X b Y c'
    >>> expand_variables(logger, "a ${first} b", {"first": "$second", "second": "value"})
    'a value b'
    """
    # The following typing.cast are due to MyPy false positive:
    # https://github.com/python/mypy/issues/12989
    if type(v) is str:
        if m := re.match(VARIABLE_REGEX, v):
            return expand_variables(logger, get_variable(logger, m, context), context)
        parts = expand_variables_as_parts(logger, v, context)
        return typing.cast(T, "".join(parts))
    if type(v) is list:
        return typing.cast(T, [expand_variables(logger, item, context) for item in v])
    if type(v) is dict:
        return typing.cast(
            T,
            {key: expand_variables(logger, value, context) for key, value in v.items()},
        )
    return v


def expand_variables_as_parts(logger: logging.LoggerAdapter, s, context):
    parts = []
    last = 0
    for m in re.finditer(VARIABLE_REGEX, s):
        begin, end = m.span()
        parts.append(s[last:begin])
        variable_value = get_variable(logger, m, context)
        if variable_value is not None:
            parts.extend(expand_variables_as_parts(logger, variable_value, context))
        last = end
    parts.append(s[last:])
    return parts


def get_variable(logger, m, context):
    variable_name = m.group("name") or m.group("curlyname")
    return lookup_variable(logger, variable_name, context)


def lookup_variable(logger: logging.LoggerAdapter, name, context):
    value = context.get(name)
    if value is None:
        logger.warning(f"Unknown variable {name}")
        return None
    return value


def compute_image(
    logger: logging.LoggerAdapter, tags: set[str], image_name: typing.Optional[str]
) -> base.Image:
    """
    Compute image from tags and image key.

    >>> logger = logging.LoggerAdapter(logging.getLogger())
    >>> compute_image(logger, {"windows"}, "alpine")
    Image(contents=CloudStack(kind='CloudStack', template_filter='community', template_name='windows-10-wsl', spec=Spec(cpu=2, ram=8, disk=16), options=Options(docker=None)))
    >>> compute_image(logger, {"linux", "docker"}, "alpine")
    Image(contents=CloudStack(kind='CloudStack', template_filter='community', template_name='ci.inria.fr/alpine-3.18.2-runner/amd64', spec=Spec(cpu=2, ram=8, disk=16), options=Options(docker='alpine')))
    """
    default_spec = find_spec_tag(SPEC_TAGS, tags)
    if default_spec is None:
        default_spec = base.DEFAULT_SPEC
    os_image = find_spec_tag(OS_TAGS, tags)
    docker = "docker" in tags
    if not docker and os_image is not None:
        image_name = os_image
    elif docker and os_image not in [None, OS_TAGS["linux"]]:
        raise Exception("docker is only available on Linux")
    elif image_name is None:
        raise Exception("image key is required")
    else:
        # Undefined variables are left as is in CI_JOB_IMAGE, but
        # should be discarded
        image_name = expand_variables(logger, image_name, {})
    image = base.parse_image_name(image_name, default_spec, base.DEFAULT_OPTIONS)
    if docker and (
        not isinstance(image.contents, base.CloudStack)
        or image.contents.options.docker is None
    ):
        raise Exception(
            "The job has docker tag but the image does not use docker container"
        )
    return image


def find_spec_tag(specs: dict[str, T], tags: set[str]) -> typing.Optional[T]:
    matches = [(key, value) for (key, value) in specs.items() if key in tags]
    match matches:
        case []:
            return None
        case [(_key, value)]:
            return value
        case _:
            tag_names = ", ".join([key for (key, _value) in matches])
            raise Exception("Conflit between mutually exclusive tags: {tag_names}")


if __name__ == "__main__":
    base.execute_with_loggers("config", main)
