#!/builds/orchestrator/bin/python

# Standard library
import logging
import multiprocessing
import os
import time

# Third-party imports
import cs
import zmq

# Project module
import base


def main(logger: logging.LoggerAdapter):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("ipc:///builds/destroy.socket")
    while True:
        message = socket.recv_string()
        if message == "unit-test/destroy-service-socket":
            logger.info("unit-test/destroy-service-socket")
            socket.send_string("ok")
            continue
        socket.send(b"")
        multiprocessing.Process(target=perform, args=(logger, message)).start()


def perform(logger: logging.LoggerAdapter, message):
    order = base.DestroyOrder.model_validate_json(message)
    try:
        cleanup_job(logger, order.job_info, order.vm_name)
    except Exception as e:
        logger.exception(f"Uncaught exception: {e}")


def cleanup_job(
    logger: logging.LoggerAdapter, job_info: base.JobInfo, vm_name: str
) -> None:
    config = base.Config.load()
    cloudstack = config.get_cloudstack()
    vm = base.find_vm_by_name(cloudstack, vm_name)
    start_time = time.perf_counter()
    base.destroy_vms(logger, cloudstack, [vm])
    ip_address = base.get_vm_ip_address(vm)
    cache_dir = base.get_cache_dir(job_info)
    base.execute(["sudo", "exportfs", "-u", f"{ip_address}:{cache_dir}"])
    end_time = time.perf_counter()
    ellapsed_time = end_time - start_time
    assert isinstance(job_info.image.contents, base.CloudStack)
    extra: dict[str, int | float] = {"destroy_vm_ellapsed_time": ellapsed_time}
    extra.update(job_info.image.contents.spec.model_dump())
    logger.info(f"Destroyed {vm_name}.", extra=extra)
    recreate_vm(logger, cloudstack, config, job_info, vm_name)


def recreate_vm(
    logger: logging.LoggerAdapter,
    cloudstack: cs.CloudStack,
    config: base.Config,
    job_info: base.JobInfo,
    vm_name: str,
) -> None:
    assert isinstance(job_info.image.contents, base.CloudStack)
    template = base.find_template(cloudstack, job_info.image.contents)
    spec = job_info.image.contents.spec.adjust_for_template(logger, template, False)
    job = base.deploy_vm(logger, cloudstack, config, vm_name, spec, template)
    start_time = time.perf_counter()
    logger.info(
        f"Recreating {vm_name}...", extra=base.collect_cloudstack_metrics(cloudstack)
    )
    vm = base.find_vm_by_id(cloudstack, job["id"])
    base.create_vm_tags(
        logger, cloudstack, vm, {base.JOB_PREPARE_KEY: base.JOB_PREPARE_KEY}
    )
    try:
        vm = base.wait_for_vm_deployment(logger, cloudstack, job)
        end_time = time.perf_counter()
        ellapsed_time = end_time - start_time
        logger.info(
            f"Recreated {vm_name}.",
            extra={
                "deploy_vm_ellapsed_time": ellapsed_time,
                "vm_spec": spec.model_dump(),
            },
        )
    except cs.client.CloudStackApiException as e:
        logger.exception(f"CloudStack error while recreating {vm_name}: {e}")
        base.destroy_vms(logger, cloudstack, [vm])


if __name__ == "__main__":
    base.execute_with_loggers("destroy", main)
