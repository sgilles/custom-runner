terraform {
  required_providers {
    cloudstack = {
      source  = "cloudstack/cloudstack"
      version = "0.4.0"
    }
  }
}

provider "cloudstack" {
  ## Provided by environment (GitLab variables)
  # api_url    = "${var.cloudstack_api_url}"
  # api_key = "${var.cloudstack_api_key}"
  # secret_key = "${var.cloudstack_secret_key}"

  timeout = 1200
}

variable "ZONE" {
  type    = string
  default = "zone-ci"
}

variable "SSH_CIDR" {
  type    = string
  default = "172.21.0.3/32"
}

variable "SSH_PUBLIC_KEY" {
  type = string
}

variable "CI_PROJECT_PATH" {
  type = string
}

variable "DATA_DISK_SIZE" {
  type    = number
  default = 40
}

variable "ORCHESTRATOR_VM_NAME" {
  type    = string
  default = null
}

variable "CPU_SPEED" {
  type    = number
  default = null
}

resource "cloudstack_security_group" "orchestrator" {
  name = "orchestrator"
}

resource "cloudstack_security_group" "runners" {
  name = "runners"
}

resource "cloudstack_security_group_rule" "orchestrator" {
  security_group_id = cloudstack_security_group.orchestrator.id

  rule {
    traffic_type = "ingress"
    protocol     = "tcp"
    ports        = ["22"]
    cidr_list    = [var.SSH_CIDR]
  }

  rule {
    traffic_type = "ingress"
    protocol     = "tcp"
    ports        = ["111", "2048-65535"]
    cidr_list    = ["172.21.0.0/16"]
  }

  rule {
    traffic_type = "ingress"
    protocol     = "udp"
    ports        = ["111", "2048-65535"]
    cidr_list    = ["172.21.0.0/16"]
  }
}

resource "cloudstack_security_group_rule" "runners" {
  security_group_id = cloudstack_security_group.runners.id

  rule {
    traffic_type             = "ingress"
    protocol                 = "tcp"
    ports                    = ["22"]
    user_security_group_list = [cloudstack_security_group.orchestrator.name]
  }
}

resource "cloudstack_instance" "orchestrator" {
  ## It is a good practice to have the "{project name}-" prefix
  ## in VM names.
  name             = var.ORCHESTRATOR_VM_NAME != null ? var.ORCHESTRATOR_VM_NAME : replace("${var.CI_PROJECT_PATH}-orchestrator", "/", "-")
  service_offering = "Custom"
  template         = "ci.inria.fr/ubuntu-23.10/amd64"
  zone             = var.ZONE
  root_disk_size   = 24
  details = var.CPU_SPEED != null ? {
    cpuNumber = 4
    memory    = 8192
    cpuSpeed  = 2000
    } : {
    cpuNumber = 4
    memory    = 8192
  }
  expunge = true
  user_data = templatefile("cloud-init.yaml.tftpl", {
    SSH_PUBLIC_KEY = var.SSH_PUBLIC_KEY
  })
  security_group_ids = [cloudstack_security_group.orchestrator.id]
  lifecycle {
    replace_triggered_by = [null_resource.orchestrator_replacement_trigger]
  }
}

resource "null_resource" "orchestrator_replacement_trigger" {
  triggers = {
    "user_data" = sha512(templatefile("cloud-init.yaml.tftpl", {
      SSH_PUBLIC_KEY = var.SSH_PUBLIC_KEY
    }))
  }
}

# We only set up one data disk because we do not know how to
# distinguish data disks on virtual machine.
# (They appear in a random order as `/dev/vd[b,c,d...]` regardless of
# the value of `device_id` and the order of their definitions.)

resource "cloudstack_disk" "data-disk" {
  name               = replace("${var.CI_PROJECT_PATH}-data-disk", "/", "-")
  attach             = "true"
  disk_offering      = "Custom"
  size               = var.DATA_DISK_SIZE
  virtual_machine_id = cloudstack_instance.orchestrator.id
  zone               = var.ZONE
}
